function main(){
	var name = request.getString("name");
	var contacts = request.getString("contacts");
	var mobile = request.getString("mobile");
	var cate_id = 1;
	var main_cate = 1;
	var grade_id = 1;
	var phone = request.getString("phone") || "";
	var province_id = request.getString("province_id");
	var city_id = request.getString("city_id");
	var county_id = request.getString("county_id");
	var address = request.getString("address");
	var org_name = request.getString("org_name");
	var describe = request.getString("describe");
	var shop_status = 0;
	var auth_status = 0;
	var create_by = 1;

	if(!name){
	  nami.error("店铺名称不能为空");
	}

	var result = db.save("insert into dc_shop (name,contacts,mobile,cate_id,main_cate,grade_id,phone,province_id,city_id,county_id,address,org_name,describe,shop_status,auth_status,del_flag,create_by,create_date) "
		+ "values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", name,contacts,mobile,cate_id,main_cate,grade_id,phone,province_id,city_id,county_id,address,org_name,describe,shop_status,auth_status,0,create_by,now);
	return {result:result};
}

main();