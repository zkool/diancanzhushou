/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2017/4/11 13:12:01                           */
/*==============================================================*/


drop table if exists dc_item;

drop table if exists dc_item_cate;

drop table if exists dc_order;

drop table if exists dc_order_item;

drop table if exists dc_shop;

drop table if exists dc_table;

drop table if exists dc_user;

/*==============================================================*/
/* Table: dc_item                                               */
/*==============================================================*/
create table dc_item
(
   id                   int(11) not null auto_increment comment 'id',
   shop_id              int(11) not null comment '店铺ID',
   cate_id              int(11) not null comment '产品分类ID',
   name                 varchar(64) not null comment '名称',
   code                 varchar(32) comment '编号',
   price                decimal(10,2) comment '单价',
   unit                 varchar(10) comment '单位',
   remark               varchar(255) comment '备注',
   status               tinyint(1) comment '状态',
   photo_path           varchar(200) comment '图片地址',
   recommend            tinyint(1) comment '推荐',
   del_flag             tinyint(1) comment '删除状态',
   create_by            int(11) comment '创建者',
   create_date          timestamp comment '创建时间',
   update_by            int(11) comment '更新者',
   update_date          timestamp comment '更新时间',
   primary key (id)
);

alter table dc_item comment '产品表';

/*==============================================================*/
/* Table: dc_item_cate                                          */
/*==============================================================*/
create table dc_item_cate
(
   id                   int(11) not null auto_increment comment 'id',
   shop_id              int(11) comment '店铺ID',
   name                 varchar(64) comment '名称',
   remark               varchar(255) comment '备注',
   create_by            int(11) comment '创建人',
   create_date          timestamp comment '创建时间',
   primary key (id)
);

alter table dc_item_cate comment '产品分类';

/*==============================================================*/
/* Table: dc_order                                              */
/*==============================================================*/
create table dc_order
(
   id                   int(11) not null auto_increment comment 'id',
   shop_id              int(11) not null comment '店铺ID',
   mode                 tinyint(1) comment '用餐方式',
   arrival_date         varchar(32) comment '到店时间',
   table_code           varchar(32) comment '桌号',
   peopel_num           int(3) comment '人数',
   amount               decimal(10,2) comment '金额',
   remark               varchar(255) comment '备注',
   invoice_title        varchar(64) comment '发票抬头',
   status               tinyint(1) comment '状态 -1 已取消 0 备餐中 1就餐中 2 完成',
   create_by            int(11) comment '创建者',
   create_date          timestamp comment '创建时间',
   pay_date             timestamp comment '支付时间',
   confirm_date         timestamp comment '确认时间',
   finish_date          timestamp comment '完成时间',
   cancel_date          timestamp comment '取消时间',
   primary key (id)
);

alter table dc_order comment '订单';

/*==============================================================*/
/* Table: dc_order_item                                         */
/*==============================================================*/
create table dc_order_item
(
   id                   int(11) not null auto_increment comment 'id',
   order_id             int(11) not null comment '订单ID',
   item_id              int(11) not null comment '产品ID',
   num                  int(6) not null comment '数量',
   price                decimal(10,2) not null comment '单价',
   primary key (id)
);

alter table dc_order_item comment '订单明细';

/*==============================================================*/
/* Table: dc_shop                                               */
/*==============================================================*/
create table dc_shop
(
   id                   int(11) not null auto_increment comment 'id',
   name                 varchar(64) not null comment '店铺名称',
   cate_id              int(11) comment '分类',
   main_cate            varchar(128) comment '主营类目',
   grade_id             int(11) comment '店铺等级',
   contacts             varchar(20) comment '联系人',
   mobile               varchar(20) comment '手机号',
   phone                varchar(20) comment '电话',
   province_id          int(11) comment '所在省',
   city_id              int(11) comment '所在市',
   county_id            int(11) comment '所在县',
   address              varchar(255) comment '详细地址',
   org_name             varchar(128) comment '公司名称',
   logo_path            varchar(200) comment 'logo',
   qrcode_path          varchar(200) comment '二维码地址',
   describe             varchar(255) comment '简介',
   shop_status          tinyint(2) comment '1 筹备中 2 营业中 3 休息中 4店铺关闭 5 不显示',
   auth_status          tinyint(2) comment '认证状态',
   del_flag             tinyint(1) comment '删除状态',
   shop_hours           varchar(20) comment '营业时间',
   sort                 int(11) comment '排序序号',
   create_by            int(11) comment '创建者',
   create_date          timestamp comment '创建时间',
   update_by            int(11) comment '更新者',
   update_date          timestamp comment '更新时间',
   primary key (id)
);

alter table dc_shop comment '店铺表';

/*==============================================================*/
/* Table: dc_table                                              */
/*==============================================================*/
create table dc_table
(
   id                   int(11) not null auto_increment comment 'id',
   shop_id              int(11) not null comment '店铺ID',
   name                 varchar(64) comment '名称',
   code                 varchar(32) comment '编号',
   remark               varchar(200) comment '备注',
   qrcode_path          varchar(200) comment '二维码地址',
   create_by            int(11) comment '创建者',
   create_date          timestamp comment '创建时间',
   primary key (id)
);

alter table dc_table comment '桌位';

/*==============================================================*/
/* Table: dc_user                                               */
/*==============================================================*/
create table dc_user
(
   id                   int(11) not null auto_increment comment 'id',
   name                 varchar(32) comment '名称',
   nick                 varchar(32) comment '昵称',
   password             varchar(128) comment '密码',
   mobile               varchar(20) comment '手机',
   email                varchar(32) comment '邮箱',
   user_type            tinyint(1) comment '0普通用户 1管理员',
   open_id              varchar(128) comment 'open_id',
   union_id             varchar(128) comment 'union_id',
   sex                  tinyint(1) comment '性别',
   country              varchar(200) comment '所在国家',
   province             varchar(200) comment '所在省',
   city                 varchar(200) comment '所在市',
   avatar_url           varchar(500) comment '头像',
   login_ip             varchar(64) comment 'login_ip',
   login_date           timestamp comment 'login_date',
   del_flag             tinyint(1) comment 'del_flag',
   create_date          timestamp comment 'create_date',
   primary key (id)
);

alter table dc_user comment '用户表';


/*==============================================================*/
/* Table: dc_area                                               */
/*==============================================================*/
create table dc_area
(
   id                   int(11) not null auto_increment,
   parent_id            int(11) not null,
   type                 tinyint(1) comment '1 省 2 市 3区/县 4 镇',
   name                 varchar(64) not null,
   code                 varchar(20),
   primary key (id)
);
